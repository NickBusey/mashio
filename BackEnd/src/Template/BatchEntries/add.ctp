<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\BatchEntry $batchEntry
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Batch Entries'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Recipe Entries'), ['controller' => 'RecipeEntries', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Recipe Entry'), ['controller' => 'RecipeEntries', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Batches'), ['controller' => 'Batches', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Batch'), ['controller' => 'Batches', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="batchEntries form large-9 medium-8 columns content">
    <?= $this->Form->create($batchEntry) ?>
    <fieldset>
        <legend><?= __('Add Batch Entry') ?></legend>
        <?php
            echo $this->Form->control('deleted', ['empty' => true]);
            echo $this->Form->control('title');
            echo $this->Form->control('recipe_entry_id', ['options' => $recipeEntries, 'empty' => true]);
            echo $this->Form->control('batch_id', ['options' => $batches, 'empty' => true]);
            echo $this->Form->control('status');
            echo $this->Form->control('date', ['empty' => true]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
