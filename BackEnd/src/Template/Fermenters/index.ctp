<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Fermenter[]|\Cake\Collection\CollectionInterface $fermenters
 */
?>
<h3><?= __('Fermenters') ?></h3>

<div class="row fermenters">
    <?php foreach ($fermenters as $fermenter): ?>
        <!-- TWITTER PANEL -->
        <div class="col-md-4 mb">
            <div class="darkblue-panel pn">
                <div class="darkblue-header">
                    <h5><a href='/fermenters/edit/<?=$fermenter->id?>'><?=$fermenter->name?></a></h5>
                </div>
                <?php if ($fermenter->batch) { ?>
                    <h2><a href='/batches/view/<?=$fermenter->batch->id?>'><?=$fermenter->batch->name?></a></h2>
                <?php } else { ?>
                    <h2>Empty</h2>
                <?php } ?>
                <footer>
                    <div class="pull-left">
                        <h5>Capacity: <?=$fermenter->capacity?></h5>
                        <?php if ($fermenter->batch) { ?>
                            <h5><i class="fa fa-tint"></i> <?=$fermenter->batch->volume?> of <?=$fermenter->capacity?> gallons</h5>
                            <h5><i class="fa fa-calendar-alt"></i> Brew Date <?=$fermenter->batch->brew_date?></h5>
                            <h5>Package Date <?=$fermenter->batch->package_date?></h5>
                        <?php } ?>
                    </div>
                    <div class="pull-right">
                        <h5>Status: <?=$this->Enum->enumKeyToValue('Fermenters','status',$fermenter->status)?></h5>
                        <h5>Type: <?=$this->Enum->enumKeyToValue('Fermenters','type',$fermenter->type)?></h5>
                        <?php if ($fermenter->batch) { ?>
                            <h5><?= $this->Number->format(round($fermenter->batch->volume/$fermenter->capacity*100)) ?>% full</h5>
                            <h5><?= $this->Calculation->getPintsPerGallons($fermenter->batch->volume)?> pints</h5>
                            <h5><?= $this->Calculation->getFermentCompletionPercentage($fermenter->batch)?>% done</h5>
                        <?php } ?>
                    </div>
                </footer>
            </div><!-- -- /darkblue panel ---->
        </div><!-- /col-md-4 -->
    <?php endforeach; ?>
</div>

<?= $this->Html->link(__('New Fermenter'), ['action' => 'add']) ?>
