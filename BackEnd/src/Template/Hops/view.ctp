<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Hop $hop
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Hop'), ['action' => 'edit', $hop->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Hop'), ['action' => 'delete', $hop->id], ['confirm' => __('Are you sure you want to delete # {0}?', $hop->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Hops'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Hop'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="hops view large-9 medium-8 columns content">
    <h3><?= h($hop->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($hop->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Usage') ?></th>
            <td><?= h($hop->usage) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($hop->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Origin') ?></th>
            <td><?= $this->Number->format($hop->origin) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Alpha Acids') ?></th>
            <td><?= $this->Number->format($hop->alpha_acids) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Beta Acids') ?></th>
            <td><?= $this->Number->format($hop->beta_acids) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Notes') ?></h4>
        <?= $this->Text->autoParagraph(h($hop->notes)); ?>
    </div>
    <div class="row">
        <h4><?= __('Substitutes') ?></h4>
        <?= $this->Text->autoParagraph(h($hop->substitutes)); ?>
    </div>
    <div class="row">
        <h4><?= __('Style Use') ?></h4>
        <?= $this->Text->autoParagraph(h($hop->style_use)); ?>
    </div>
</div>
