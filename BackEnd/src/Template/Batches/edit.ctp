<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Batch $batch
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $batch->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $batch->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Batches'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Recipes'), ['controller' => 'Recipes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Recipe'), ['controller' => 'Recipes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Batch Entries'), ['controller' => 'BatchEntries', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Batch Entry'), ['controller' => 'BatchEntries', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Measurements'), ['controller' => 'Measurements', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Measurement'), ['controller' => 'Measurements', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="batches form large-9 medium-8 columns content">
    <?= $this->Form->create($batch) ?>
    <fieldset>
        <legend><?= __('Edit Batch') ?></legend>
        <?php
            echo $this->Form->select('status', $this->Enum->selectValues('Batches', 'status'));
            echo $this->Form->control('last_sg');
            echo $this->Form->control('volume');
            echo $this->Form->control('og');
            echo $this->Form->control('fg');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
