<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Batch $batch
 */
?>
<h3><i class="fa fa-angle-right"></i> Create New Batch</h3>

<div class="col-lg-12">
    <div class="form-panel">
        <?= $this->Form->create($batch,['class'=>'form-horizontal style-form']) ?>
            <div class="form-group">
                <label class="control-label col-md-3">Recipe</label>
                <div class="col-md-3 col-xs-11">
                    <?=$this->Form->control('recipe_id', ['options' => $recipes, 'empty' => false, 'label'=>false]);?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3">Planned Brew Date</label>
                <div class="col-md-3 col-xs-11">
                    <?php
                    // Default brew date to one week from current date
                    use Cake\I18n\Date;
                    $next_week = new Date('+1 week');
                    ?>
                    <input class="form-control form-control-inline input-medium date-picker" size="16" name="brew_date" type="text" value="<?=$next_week->format('Y-m-d')?>">
                    <span class="help-block">Select date</span>
                </div>
            </div>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </form>
    </div><!-- /form-panel -->
</div>
