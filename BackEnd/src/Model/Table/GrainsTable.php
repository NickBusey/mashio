<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Grains Model
 *
 * @property \App\Model\Table\AttributesTable|\Cake\ORM\Association\BelongsTo $Attributes
 *
 * @method \App\Model\Entity\Grain get($primaryKey, $options = [])
 * @method \App\Model\Entity\Grain newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Grain[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Grain|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Grain|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Grain patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Grain[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Grain findOrCreate($search, callable $callback = null, $options = [])
 */
class GrainsTable extends Table
{
    // @codeCoverageIgnoreStart
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('grains');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->belongsTo('Attributes', [
            'foreignKey' => 'attributes_id'
        ]);

        $this->addBehavior('Search.Search');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->integer('origin')
            ->requirePresence('origin', 'create')
            ->notEmpty('origin');

        $validator
            ->decimal('yield')
            ->requirePresence('yield', 'create')
            ->notEmpty('yield');

        $validator
            ->scalar('potential')
            ->maxLength('potential', 255)
            ->requirePresence('potential', 'create')
            ->notEmpty('potential');

        $validator
            ->decimal('color')
            ->requirePresence('color', 'create')
            ->notEmpty('color');

        $validator
            ->decimal('protein')
            ->requirePresence('protein', 'create')
            ->notEmpty('protein');

        $validator
            ->boolean('mash')
            ->allowEmpty('mash');

        $validator
            ->scalar('notes')
            ->maxLength('notes', 16777215)
            ->allowEmpty('notes');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['attributes_id'], 'Attributes'));

        return $rules;
    }
}
