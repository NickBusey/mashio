<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Fermenters Model
 *
 * @property \App\Model\Table\BatchesTable|\Cake\ORM\Association\BelongsTo $Batches
 *
 * @method \App\Model\Entity\Fermenter get($primaryKey, $options = [])
 * @method \App\Model\Entity\Fermenter newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Fermenter[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Fermenter|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Fermenter|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Fermenter patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Fermenter[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Fermenter findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class FermentersTable extends Table
{
    public $enums = array(
        'status' => array(
            'Empty (Dirty)',
            'Empty (Clean)',
            'Empty (Sanitized)',
            'Fermenting Primary',
            'Fermenting Secondary',
            'Tapped',
            'Filled w/ Caustic',
            'Filled w/ Water',
            'Filled w/ Sanitizer',
        ),
        'type' => [
            'Plastic Bucket',
            'Glass Carboy',
            'Plastic Carboy',
            'Corny Keg',
            'Conical',
            'Sanke Keg',
        ]
    );

    /**
     * Initialize method
     *
     * @param  array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('fermenters');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Search.Search');

        $this->hasOne(
            'Batches', [
            'foreignKey' => 'fermenter_id'
            ]
        );

        $this->addBehavior('Enum');

        $this->addBehavior('Muffin/Trash.Trash');
    }

    /**
     * Default validation rules.
     *
     * @param  \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->dateTime('deleted')
            ->allowEmpty('deleted');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->allowEmpty('name');

        $validator
            ->scalar('description')
            ->allowEmpty('description');

        $validator
            ->numeric('capacity')
            ->allowEmpty('capacity');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param  \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['batch_id'], 'Batches'));

        return $rules;
    }
}
