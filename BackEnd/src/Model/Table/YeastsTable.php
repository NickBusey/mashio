<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Yeasts Model
 *
 * @property \App\Model\Table\ProductsTable|\Cake\ORM\Association\BelongsTo $Products
 *
 * @method \App\Model\Entity\Yeast get($primaryKey, $options = [])
 * @method \App\Model\Entity\Yeast newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Yeast[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Yeast|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Yeast|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Yeast patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Yeast[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Yeast findOrCreate($search, callable $callback = null, $options = [])
 */
class YeastsTable extends Table
{
    // @codeCoverageIgnoreStart
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('yeasts');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->belongsTo('Products', [
            'foreignKey' => 'product_id',
            'joinType' => 'INNER'
        ]);

        $this->addBehavior('Search.Search');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->scalar('lab')
            ->maxLength('lab', 255)
            ->requirePresence('lab', 'create')
            ->notEmpty('lab');

        $validator
            ->scalar('style')
            ->maxLength('style', 255)
            ->requirePresence('style', 'create')
            ->notEmpty('style');

        $validator
            ->scalar('floculation')
            ->maxLength('floculation', 255)
            ->requirePresence('floculation', 'create')
            ->notEmpty('floculation');

        $validator
            ->scalar('temp')
            ->maxLength('temp', 255)
            ->requirePresence('temp', 'create')
            ->notEmpty('temp');

        $validator
            ->scalar('notes')
            ->maxLength('notes', 16777215)
            ->requirePresence('notes', 'create')
            ->notEmpty('notes');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['product_id'], 'Products'));

        return $rules;
    }
}
