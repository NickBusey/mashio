<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Taps Model
 *
 * @property \App\Model\Table\BatchesTable|\Cake\ORM\Association\BelongsTo $Batches
 *
 * @method \App\Model\Entity\Tap get($primaryKey, $options = [])
 * @method \App\Model\Entity\Tap newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Tap[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Tap|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Tap|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Tap patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Tap[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Tap findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TapsTable extends Table
{
    public $enums = [
        'status' => [
            'Empty (Dirty)',
            'Empty (Cleaned)',
            'Empty (Sanitized)',
            'Tapped'
        ]
    ];

    /**
     * Initialize method
     *
     * @param  array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('taps');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasOne(
            'Batches', [
                'foreignKey' => 'tap_id'
            ]
        );

        $this->addBehavior('Enum');

        $this->addBehavior('Muffin/Trash.Trash');
        $this->addBehavior('Search.Search');
    }

    // @codeCoverageIgnoreStart

    /**
     * Default validation rules.
     *
     * @param  \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->dateTime('deleted')
            ->allowEmpty('deleted');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->allowEmpty('name');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param  \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {

        return $rules;
    }
}
