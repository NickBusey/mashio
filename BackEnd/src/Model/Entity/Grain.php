<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Grain Entity
 *
 * @property int $id
 * @property string $name
 * @property int $origin
 * @property float $yield
 * @property string $potential
 * @property float $color
 * @property float $protein
 * @property bool|null $mash
 * @property string|null $notes
 * @property string|null $attributes_id
 *
 * @property \App\Model\Entity\Attribute $attribute
 */
class Grain extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'origin' => true,
        'yield' => true,
        'potential' => true,
        'color' => true,
        'protein' => true,
        'mash' => true,
        'notes' => true,
        'attributes_id' => true,
        'attribute' => true
    ];
}
