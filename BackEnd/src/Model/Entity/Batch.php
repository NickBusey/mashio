<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\I18n\Date;

/**
 * Batch Entity
 *
 * @property int $id
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 * @property int|null $status
 * @property int|null $recipe_id
 * @property \Cake\I18n\FrozenDate|null $brew_date
 * @property float|null $last_sg
 * @property float|null $volume
 * @property float|null $og
 * @property float|null $fg
 * @property \Cake\I18n\FrozenDate|null $package_date
 *
 * @property \App\Model\Entity\Recipe $recipe
 * @property \App\Model\Entity\BatchEntry[] $batch_entries
 * @property \App\Model\Entity\Measurement[] $measurements
 */
class Batch extends Entity
{
    protected $_virtual = ['name','estimated_remaining','days_tapped'];

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*'=>true,'id'=>false
    ];

    protected function _getName()
    {
        if (isset($this->_properties['custom_name'])) {
            return (
                $this->_properties['brew_date'] ? 
                $this->_properties['brew_date']->format('m-d') . ' ' . $this->_properties['custom_name'] : 
                $this->_properties['custom_name']
            );
        } else if (isset($this->_properties['recipe'])) {
            return (
                $this->_properties['brew_date'] ? 
                $this->_properties['brew_date']->format('m-d') . ' ' . $this->_properties['recipe']['name'] : 
                $this->_properties['recipe']['name']
            );
        } else if (isset($this->_properties['brew_date'])) {
            return $this->_properties['brew_date']->format('m-d');
        } else {
            return 'Unknown Batch';
        }
    }

    protected function _getEstimatedRemaining()
    {
        $averageKickedDaysRate = 31;
        $daysTapped = $this->days_tapped;
        if ($daysTapped) {
            return round($daysTapped / $averageKickedDaysRate * 100, 2);
        }
    }

    protected function _getDaysTapped()
    {
        if (isset($this->_properties['tapped_date'])) {
            if (isset($this->_properties['kicked_date'])) {
                return date_diff($this->_properties['tapped_date'], $this->_properties['kicked_date'])->format('%a');
            } else {
                return date_diff($this->_properties['tapped_date'], new \DateTime("now"))->format('%a');
            }
        }
    }

    protected function _getPints()
    {
        // TODO: Make this not assume gallons
        return $this->_properties['volume'] * 8;
    }

}
