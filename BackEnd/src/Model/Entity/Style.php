<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Style Entity
 *
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 * @property string|null $name
 * @property string|null $bjcp_number
 * @property int|null $og_min
 * @property int|null $og_max
 * @property int|null $fg_min
 * @property int|null $fg_max
 * @property int|null $ibu_min
 * @property int|null $ibu_max
 * @property int|null $srm_min
 * @property int|null $srm_max
 * @property int|null $abv_min
 * @property int|null $abv_max
 * @property string|null $aroma
 * @property string|null $appearance
 * @property string|null $flavor
 * @property string|null $mouthfeel
 * @property string|null $overall
 * @property string|null $comments
 * @property string|null $ingredients
 *
 * @property \App\Model\Entity\Recipe[] $recipes
 */
class Style extends Entity
{
    protected $_virtual = ['color'];
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'name' => true,
        'bjcp_number' => true,
        'og_min' => true,
        'og_max' => true,
        'fg_min' => true,
        'fg_max' => true,
        'ibu_min' => true,
        'ibu_max' => true,
        'srm_min' => true,
        'srm_max' => true,
        'abv_min' => true,
        'abv_max' => true,
        'aroma' => true,
        'appearance' => true,
        'flavor' => true,
        'mouthfeel' => true,
        'overall' => true,
        'comments' => true,
        'ingredients' => true,
        'recipes' => true
    ];

    // Get HTML color based on SRM
    public function _getColor()
    {
        if (!isset($this->_properties['srm_max'])) {
            return;
        }
        $srm = $this->_properties['srm_max'];

        if ($srm < 3) {
            // 2
            return '#ffff45';
            // Testing all the rest of these would be tedious.
            // @codeCoverageIgnoreStart
        } else if ($srm > 2 && $srm < 4) {
            // 3
            return '#ffe93e';
        } else if ($srm > 3 && $srm < 5) {
            // 4
            return '#fed849';
        } else if ($srm > 4 && $srm < 7) {
            // 6
            return '#ffa846';
        } else if ($srm > 6 && $srm < 10) {
            // 9
            return '#f49f44';
        } else if ($srm > 9 && $srm < 13) {
            // 12
            return '#d77f59';
        } else if ($srm > 12 && $srm < 16) {
            // 15
            return '#94523a';
        } else if ($srm > 15 && $srm < 19) {
            // 18
            return '#804541';
        } else if ($srm > 18 && $srm < 21) {
            // 20
            return '#5b342f';
        } else if ($srm > 20 && $srm < 25) {
            // 24
            return '#5b342f';
        } else if ($srm > 24 && $srm < 31) {
            // 30
            return '#38302e';
        } else if ($srm > 30) {
            // 40
            return '#31302c';
        }
    }
    // @codeCoverageIgnoreEnd
}
