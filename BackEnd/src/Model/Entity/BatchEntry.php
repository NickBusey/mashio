<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;

/**
 * BatchEntry Entity
 *
 * @property int $id
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 * @property string|null $title
 * @property int|null $recipe_entry_id
 * @property int|null $batch_id
 * @property int|null $status
 * @property \Cake\I18n\FrozenTime|null $date
 *
 * @property \App\Model\Entity\RecipeEntry $recipe_entry
 * @property \App\Model\Entity\Batch $batch
 */
class BatchEntry extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*'=>true,'id'=>false
    ];

    // Get the date for the entry, either planned, or actual
    protected function _getDisplayDate()
    {
        $this->BatchEntries = TableRegistry::get('BatchEntries');
        $this->Batches = TableRegistry::get('Batches');
        $this->RecipeEntries = TableRegistry::get('RecipeEntries');
        if ($this->_properties['status'] == $this->BatchEntries->enumValueToKey('status', 'Planned')) {
            $batch = $this->Batches->get($this->_properties['batch_id']);
            return $batch->brew_date->addDays($this->_properties['days']);
        } else {
            return $this->_properties['completed_date'];
        }
    }
}
