<?php

namespace App\Controller\Traits;


/**
 * Only allows interacting with records owned by the current user.
 */
trait OwnRecordsTrait
{
    public function beforeFilter(\Cake\Event\Event $event) {
        parent::beforeFilter($event);
    
        $this->Crud->on(
          'beforePaginate', function (\Cake\Event\Event $event) {
              $this->paginate['conditions'][$this->modelClass . '.user_id'] = $this->Auth->user('id');
          }
        );
        
        $this->Crud->on(
          'beforeFind', function (\Cake\Event\Event $event) {
              $event->getSubject()->query->where([$this->modelClass . '.user_id' => $this->Auth->user('id')]);
          }
        );
        $this->Crud->on(
          'beforeSave', function (\Cake\Event\Event $event) {
            if ($event->getSubject()->entity->isNew()) {
              # Store the user_id of the current user on new object creation
              $event->getSubject()->entity->user_id = $this->Auth->user('id');
            } else if ($event->getSubject()->entity->author !== $this->Auth->user('id')) {
              # Don't let users delete records belonging to other users
              $event->stopPropagation();
            }
          }
        );
        $this->Crud->on(
          'beforeDelete', function (\Cake\Event\Event $event) {
              $event->getSubject()->query->where([$this->modelClass . '.user_id' => $this->Auth->user('id')]);
          }
        );
      }
}
