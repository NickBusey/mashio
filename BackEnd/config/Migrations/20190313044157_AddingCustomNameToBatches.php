<?php
use Migrations\AbstractMigration;

class AddingCustomNameToBatches extends AbstractMigration
{

    public function up()
    {

        $this->table('batches')
            ->addColumn('custom_name', 'string', [
                'after' => 'tap_id',
                'default' => null,
                'length' => 255,
                'null' => true,
            ])
            ->update();
    }

    public function down()
    {

        $this->table('batches')
            ->removeColumn('custom_name')
            ->update();
    }
}

