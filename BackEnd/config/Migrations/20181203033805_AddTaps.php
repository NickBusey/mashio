<?php
use Migrations\AbstractMigration;

class AddTaps extends AbstractMigration
{

    public function up()
    {

        $this->table('taps')
            ->addColumn('status', 'integer', [
                'after' => 'batch_id',
                'default' => null,
                'length' => 11,
                'null' => true,
            ])
            ->update();
    }

    public function down()
    {

        $this->table('taps')
            ->removeColumn('status')
            ->update();
    }
}

