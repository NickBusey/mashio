<?php
use Migrations\AbstractMigration;

class MoveTapBatchRelationship extends AbstractMigration
{

    public function up()
    {
        $this->table('taps')
            ->removeColumn('batch_id')
            ->update();

        $this->table('batches')
            ->addColumn('tap_id', 'uuid', [
                'after' => 'fermenter_id',
                'default' => null,
                'length' => null,
                'null' => true,
            ])
            ->update();
    }

    public function down()
    {
        $this->table('batches')
            ->removeColumn('tap_id')
            ->update();

        $this->table('taps')
            ->addColumn('batch_id', 'uuid', [
                'after' => 'name',
                'default' => null,
                'length' => null,
                'null' => true,
            ])
            ->update();
    }
}

