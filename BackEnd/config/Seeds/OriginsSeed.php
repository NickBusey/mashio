<?php
use Migrations\AbstractSeed;

/**
 * Origins seed.
 */
class OriginsSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'name' => 'Austria',
                'short_name' => 'AT',
            ],
            [
                'id' => '2',
                'name' => 'Belgium',
                'short_name' => 'BE',
            ],
            [
                'id' => '3',
                'name' => 'Canada',
                'short_name' => 'CA',
            ],
            [
                'id' => '4',
                'name' => 'Czech Republic',
                'short_name' => 'CZ',
            ],
            [
                'id' => '5',
                'name' => 'England',
                'short_name' => 'UK',
            ],
            [
                'id' => '6',
                'name' => 'Germany',
                'short_name' => 'DE',
            ],
            [
                'id' => '7',
                'name' => 'United Kingdom',
                'short_name' => 'UK',
            ],
            [
                'id' => '8',
                'name' => 'United States',
                'short_name' => 'US',
            ],
        ];

        $table = $this->table('origins');
        $table->insert($data)->save();
    }
}
