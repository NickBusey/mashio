<?php
use Migrations\AbstractSeed;

/**
 * Categories seed.
 */
class CategoriesSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $table = $this->table('categories');

        $table->truncate();

        $data = [
            [
                'id' => '1',
                'name' => 'Standard American Beer',
                'times_brewed' => '3',
            ],
            [
                'id' => '2',
                'name' => 'International Lager',
                'times_brewed' => NULL,
            ],
            [
                'id' => '3',
                'name' => 'Czech Lager',
                'times_brewed' => NULL,
            ],
            [
                'id' => '4',
                'name' => 'Pale Malty European Lager',
                'times_brewed' => NULL,
            ],
            [
                'id' => '5',
                'name' => 'Pale Bitter European Beer',
                'times_brewed' => NULL,
            ],
            [
                'id' => '6',
                'name' => 'Amber Malty European Lager',
                'times_brewed' => NULL,
            ],
            [
                'id' => '7',
                'name' => 'Amber Bitter European Beer',
                'times_brewed' => NULL,
            ],
            [
                'id' => '8',
                'name' => 'Dark European Lager',
                'times_brewed' => NULL,
            ],
            [
                'id' => '9',
                'name' => 'Strong European Beer',
                'times_brewed' => NULL,
            ],
            [
                'id' => '10',
                'name' => 'German Wheat Beer',
                'times_brewed' => NULL,
            ],
            [
                'id' => '11',
                'name' => 'British Bitter',
                'times_brewed' => NULL,
            ],
            [
                'id' => '12',
                'name' => 'Pale Commonwealth Beer',
                'times_brewed' => NULL,
            ],
            [
                'id' => '13',
                'name' => 'Brown British Beer',
                'times_brewed' => NULL,
            ],
            [
                'id' => '14',
                'name' => 'Scottish Ale',
                'times_brewed' => NULL,
            ],
            [
                'id' => '15',
                'name' => 'Irish Beer',
                'times_brewed' => NULL,
            ],
            [
                'id' => '16',
                'name' => 'Dark British Beer',
                'times_brewed' => NULL,
            ],
            [
                'id' => '17',
                'name' => 'Strong British Ale',
                'times_brewed' => NULL,
            ],
            [
                'id' => '18',
                'name' => 'Pale American Ale',
                'times_brewed' => NULL,
            ],
            [
                'id' => '19',
                'name' => 'Amber and Brown American Beer',
                'times_brewed' => NULL,
            ],
            [
                'id' => '20',
                'name' => 'American Porter and Stout',
                'times_brewed' => NULL,
            ],
            [
                'id' => '21',
                'name' => 'IPA',
                'times_brewed' => NULL,
            ],
            [
                'id' => '22',
                'name' => 'Strong American Ale',
                'times_brewed' => NULL,
            ],
            [
                'id' => '23',
                'name' => 'European Sour Ale',
                'times_brewed' => NULL,
            ],
            [
                'id' => '24',
                'name' => 'Belgian Ale',
                'times_brewed' => NULL,
            ],
            [
                'id' => '25',
                'name' => 'Strong Belgian Ale',
                'times_brewed' => NULL,
            ],
            [
                'id' => '26',
                'name' => 'Trappist Ale',
                'times_brewed' => NULL,
            ],
            [
                'id' => '27',
                'name' => 'Historical Beer',
                'times_brewed' => NULL,
            ],
            [
                'id' => '28',
                'name' => 'American Wild Ale',
                'times_brewed' => NULL,
            ],
            [
                'id' => '29',
                'name' => 'Fruit Beer',
                'times_brewed' => NULL,
            ],
            [
                'id' => '30',
                'name' => 'Spiced Beer',
                'times_brewed' => NULL,
            ],
            [
                'id' => '31',
                'name' => 'Alternative Fermentables Beer',
                'times_brewed' => NULL,
            ],
            [
                'id' => '32',
                'name' => 'Smoked Beer',
                'times_brewed' => NULL,
            ],
            [
                'id' => '33',
                'name' => 'Wood Beer',
                'times_brewed' => NULL,
            ],
            [
                'id' => '34',
                'name' => 'Specialty Beer',
                'times_brewed' => NULL,
            ],
        ];

        $table->insert($data)->save();
    }
}
