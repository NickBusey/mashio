<?php
use Cake\Core\Configure;
use Cake\Routing\Router;
return [
  'Users' => [
        // Controller used to manage users plugin features & actions
        'controller' => 'CakeDC/Users.Users',
        // configure Auth component
        'auth' => true,
        'Email' => [
            // determines if the user should include email
            'required' => false,
            // determines if registration workflow includes email validation
            'validate' => false,
        ],
        'Registration' => [
            // determines if the register is enabled
            'active' => true,
            // determines if the reCaptcha is enabled for registration
            'reCaptcha' => false,
            //ensure user is active (confirmed email) to reset his password
            'ensureActive' => false,
            // default role name used in registration
            'defaultRole' => 'user',
        ],
        'Tos' => [
            // determines if the user should include tos accepted
            'required' => false,
        ],
        'Social' => [
            // enable social login
            'login' => false,
        ],
        // Avatar placeholder
        'Avatar' => ['placeholder' => 'CakeDC/Users.avatar_placeholder.png'],
        'RememberMe' => [
            // configure Remember Me component
            'active' => true,
        ],
    ],
// default configuration used to auto-load the Auth Component, override to change the way Auth works
    'Auth' => [
        'authenticate' => [
            'all' => [
                'finder' => 'all',
            ],
            'CakeDC/Auth.RememberMe',
            'Form',
            'CakeDC/Auth.ApiKey' => [
                'name' => 'api_key',
                'require_ssl' => false
            ]
        ],
        'authorize' => [
            'CakeDC/Auth.Superuser',
            'CakeDC/Auth.SimpleRbac' => [
              'permissions' => [
                ['role'=>'user','controller'=>'BatchEntries','action'=>'*'],
                ['role'=>'user','controller'=>'Batches','action'=>'*'],
                ['role'=>'user','controller'=>'Calculators','action'=>'*'],
                ['role'=>'user','controller'=>'Dashboards','action'=>'*'],
                ['role'=>'user','controller'=>'Fermenters','action'=>'*'],
                ['role'=>'user','controller'=>'Grains','action'=>'*'],
                ['role'=>'user','controller'=>'Hops','action'=>'*'],
                ['role'=>'user','controller'=>'Measurements','action'=>'*'],
                ['role'=>'user','controller'=>'Pages','action'=>'*'],
                ['role'=>'user','controller'=>'RecipeEntries','action'=>'*'],
                ['role'=>'user','controller'=>'Recipes','action'=>'*'],
                ['role'=>'user','controller'=>'Styles','action'=>'*'],
                ['role'=>'user','controller'=>'Taps','action'=>'*'],
                ['role'=>'user','controller'=>'Yeasts','action'=>'*'],
              ]
            ],
        ],
    ],
];
