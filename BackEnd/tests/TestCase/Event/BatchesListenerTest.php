<?php
namespace App\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Cake\Event\EventList;
use App\Event\BatchesListener;

/**
 * App\Model\Table\BatchesListener Test Case
 */
class BatchesListenerTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Event\BatchesListener
     */
    public $Batches;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.batches',
        'app.batch_entries',
        'app.recipe_entries',
        'app.recipes',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();

        $this->Batches = TableRegistry::get('Batches');
        $this->Recipes = TableRegistry::get('Recipes');
        $this->RecipeEntries = TableRegistry::get('RecipeEntries');
        $this->BatchEntries = TableRegistry::get('BatchEntries');
        // enable event tracking
        $this->Batches->getEventManager()->setEventList(new EventList());
        $this->listener = new BatchesListener();

        $this->recipe = $this->Recipes->newEntity([]);
        $this->Recipes->save($this->recipe);

        $this->batch = $this->Batches->newEntity([
          'recipe_id' => $this->recipe->id
        ]);
        $this->Batches->save($this->batch);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Batches);

        parent::tearDown();
    }

    public function testBrewing() {
      $this->recipe_entry = $this->RecipeEntries->newEntity([
        'recipe_id' => $this->recipe->id,
        'type' => $this->RecipeEntries->enumValueToKey('type', 'Heat Strike Water'),
      ]);
      $this->RecipeEntries->save($this->recipe_entry);

      $batchEntry = $this->BatchEntries->newEntity([
        'batch_id' => $this->batch->id,
        'recipe_entry_id' => $this->recipe_entry->id,
      ]);
      $this->BatchEntries->save($batchEntry);
      $batchEntry->recipe_entry = $this->recipe_entry;

      $this->listener->handleCompletedBatchEntry(false, $batchEntry);

      $batch = $this->Batches->get($this->batch->id);
      $this->assertEquals($batch->status,$this->Batches->enumValueToKey('status', 'Brewing'));
    }

    public function testFermenting() {
      $this->recipe_entry = $this->RecipeEntries->newEntity([
        'recipe_id' => $this->recipe->id,
        'type' => $this->RecipeEntries->enumValueToKey('type', 'Pitch Yeast'),
      ]);
      $this->RecipeEntries->save($this->recipe_entry);

      $batchEntry = $this->BatchEntries->newEntity([
        'batch_id' => $this->batch->id,
        'recipe_entry_id' => $this->recipe_entry->id,
      ]);
      $this->BatchEntries->save($batchEntry);
      $batchEntry->recipe_entry = $this->recipe_entry;

      $this->listener->handleCompletedBatchEntry(false, $batchEntry);

      $batch = $this->Batches->get($this->batch->id);
      $this->assertEquals($batch->status,$this->Batches->enumValueToKey('status', 'Fermenting'));
    }

    public function testPackaged() {
      $this->recipe_entry = $this->RecipeEntries->newEntity([
        'recipe_id' => $this->recipe->id,
        'type' => $this->RecipeEntries->enumValueToKey('type', 'Package'),
      ]);
      $this->RecipeEntries->save($this->recipe_entry);

      $batchEntry = $this->BatchEntries->newEntity([
        'batch_id' => $this->batch->id,
        'recipe_entry_id' => $this->recipe_entry->id,
      ]);
      $this->BatchEntries->save($batchEntry);
      $batchEntry->recipe_entry = $this->recipe_entry;

      $this->listener->handleCompletedBatchEntry(false, $batchEntry);

      $batch = $this->Batches->get($this->batch->id);
      $this->assertEquals($batch->status,$this->Batches->enumValueToKey('status', 'Packaged'));
    }
}
