[![codecov](https://codecov.io/gl/NickBusey/mashio/branch/master/graph/badge.svg)](https://codecov.io/gl/NickBusey/mashio)

# Mashio

http://mashio.beer

Mashio is meant to help home brewers manage their various ferments and recipes.

![](screenshot.png)

## Goals

* Self Hostable - No need to worry about a hosted service shutting down and losing all your data.
* API First - Strong API support will allow for easy development of third party integrations.
* 100% Test Coverage - Every line of code in the repo should be fully tested.

## [Documentation](https://nickbusey.gitlab.io/mashio/)

## Development Status

Things are still fairly early right now and changing fast.

### [Planned Features](https://gitlab.com/NickBusey/mashio/issues?label_name%5B%5D=Improvement)

## Application Design

Mashio is an API back end, combined with a React front end. The back end is built on
CakePHP, and the CRUD and JSON-CRUD-API plugins. The front end is built on [Ant Design Pro](https://pro.ant.design/).

## Support

* [File an issue](https://gitlab.com/NickBusey/mashio/issues/new)
* [Reddit](https://www.reddit.com/r/Mashio/)

## Installation

### Docker

Run `docker-compose up` to spin up a development stack.

Run `./bootstrap.sh` to bootstrap the database.

Load http://localhost:8181 to access the CakePHP backend and http://localhost:8000 to access the React front end.

## Credits

Icon made by [smalllikeart](https://www.flaticon.com/authors/smalllikeart) is licensed by CC 3.0 BY