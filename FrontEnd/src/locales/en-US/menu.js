export default {
  'menu.home': 'Home',
  'menu.dashboard': 'Dashboard',
  'menu.dashboard.fermenters': 'Fermenters',
  'menu.dashboard.taps': 'Taps',
  'menu.dashboard.batches': 'Batches',
  'menu.batchesView': 'Batch View',
  'menu.recipes': 'Recipes',
  'menu.recipes.browse': 'Browse Recipes',
  'menu.recipes.browse-styles': 'Browse Styles',
  'menu.ingredients': 'Ingredients',
  'menu.ingredients.hops': 'Browse Hops',

  'menu.account': 'Account',
  'menu.account.center': 'Account Center',
  'menu.account.settings': 'Account Settings',
  'menu.account.trigger': 'Trigger Error',
  'menu.account.logout': 'Logout',
};
