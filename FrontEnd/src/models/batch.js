import { recordIndex, recordView, tableEnums, removeRecord, addRecord, updateRecord } from '@/services/api';

export default {
  namespace: 'batches',

  state: {
    batches: [],
  },

  effects: {
    *index({ payload }, { call, put }) {
      // Get the index listing
      const response = yield call(recordIndex, payload, 'batches');
      response.data.meta = response.meta;

      const enums = yield call(tableEnums, 'batches');
      response.data.enums = enums;

      yield put({
        type: 'queryList',
        payload: Array.isArray(response.data) ? response.data : [],
      });
    },
    *view({ payload }, { call, put }) {
      const response = yield call(recordView, payload, 'batches');
      yield put({
        type: 'queryBatch',
        payload: response,
      });
    },
    *appendFetch({ payload }, { call, put }) {
      const response = yield call(recordIndex, payload, 'batches');
      yield put({
        type: 'appendList',
        payload: Array.isArray(response.data) ? response.data : [],
      });
    },
    *submit({ payload }, { call, put }) {
      let callback;
      if (payload.id) {
        callback = (payload.delete == true) ? removeRecord : updateRecord;
      } else {
        callback = addRecord;
      }
      let record = payload;
      
      const response = yield call(callback, payload, 'batches'); // post

      yield put({
        type: 'queryBatch',
        payload: response,
      });
    },
  },

  reducers: {
    queryList(state, action) {
      if (Array.isArray(action.payload)) {
        action.payload.forEach((item, index) => {
          item.attributes.statusesText = action.payload.enums.statuses[item.attributes.status]
          item.enums = action.payload.enums
        });

        return {
          ...state,
          batches: action.payload,
        };
      } else if (Array.isArray(action.payload.data)) {
        action.payload.data.meta = action.payload.meta
        action.payload.data.included = action.payload.included
        return {
          ...state,
          batches: action.payload.data,
        };
      } else {
        return state
      }
    },
    queryBatch(state, action) {
      if (action.payload.data.relationships) {
        var fermenter = action.payload.included.find(obj => {
          return obj.type === "fermenters" && obj.id === action.payload.data.relationships.fermenter.data.id;
        })
        action.payload.data.fermenter = fermenter;
      }

      let batches = state.batches
      let batchUpdated = false
      batches.forEach((batch, index) => {
        if (batch.id == action.payload.data.id) {
          batches[index] = action.payload.data
          batchUpdated = true
        }
      })

      if (!batchUpdated) {
        batches.unshift(action.payload.data)
        batches.meta.record_count++
      }
      return {
        ...state,
        batches: batches,
      };
    },
  },
};
