import { recordIndex, tableEnums, removeRecord, addRecord, updateRecord } from '@/services/api';

export default {
  namespace: 'hop',

  state: {
    hop: [],
  },

  effects: {
    *fetch({ payload }, { call, put }) {
      // Get the index listing
      const response = yield call(recordIndex, payload, 'hops');
      response.data.meta = response.meta;
      yield put({
        type: 'queryList',
        payload: Array.isArray(response.data) ? response.data : [],
      });
    },
    *appendFetch({ payload }, { call, put }) {
      const response = yield call(recordIndex, payload, 'hops');
      yield put({
        type: 'appendList',
        payload: Array.isArray(response.data) ? response.data : [],
      });
    },
    *submit({ payload }, { call, put }) {
      let callback;
      if (payload.id) {
        callback = (payload.delete == true) ? removeRecord : updateRecord;
      } else {
        callback = addRecord;
      }
      let record = payload;
      
      const response = yield call(callback, payload, 'hops'); // post

      // Get record index to overwrite everything.
      // Not the most efficient, could just update the needed record. Works for now though.
      const records = yield call(recordIndex, payload, 'hops');

      yield put({
        type: 'queryList',
        payload: records,
      });
    },
  },

  reducers: {
    queryList(state, action) {
      if (Array.isArray(action.payload)) {
        return {
          ...state,
          hop: action.payload,
        };
      } else if (Array.isArray(action.payload.data)) {
        action.payload.data.meta = action.payload.meta
        action.payload.data.included = action.payload.included
        return {
          ...state,
          hop: action.payload.data,
        };
      } else {
        return state
      }
    },
    appendList(state, action) {
      return {
        ...state,
        hop: state.hop.concat(action.payload),
      };
    },
  },
};
