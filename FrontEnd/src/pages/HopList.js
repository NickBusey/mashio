import React, { PureComponent } from 'react';
import { findDOMNode } from 'react-dom';
import moment from 'moment';
import { connect } from 'dva';
import {
  List,
  Card,
  Row,
  Col,
  Radio,
  Input,
  Progress,
  Button,
  Icon,
  Dropdown,
  Menu,
  Avatar,
  Modal,
  Form,
  DatePicker,
  Select,
} from 'antd';

import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import Result from '@/components/Result';

import styles from './List.less';

const FormItem = Form.Item;
const RadioButton = Radio.Button;
const RadioGroup = Radio.Group;
const SelectOption = Select.Option;
const { Search, TextArea } = Input;

@connect(({ hop, loading }) => ({
  hop,
  loading: loading.models.hop,
}))
@Form.create()
class HopList extends PureComponent {
  state = { visible: false, done: false };

  formLayout = {
    labelCol: { span: 7 },
    wrapperCol: { span: 13 },
  };

  pagination = {
    sort: 'name',
    limit: 10,
    direction: 'asc',
    page: 1
  }

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({
      type: 'hop/fetch',
      payload: {
        ...this.pagination
      },
    });
  }

  showModal = () => {
    this.setState({
      visible: true,
      current: undefined,
    });
  };

  showEditModal = item => {
    this.setState({
      visible: true,
      current: item,
    });
  };

  handleDone = () => {
    setTimeout(() => this.addBtn.blur(), 0);
    this.setState({
      done: false,
      visible: false,
    });
  };

  handleCancel = () => {
    setTimeout(() => this.addBtn.blur(), 0);
    this.setState({
      visible: false,
    });
  };

  handleSubmit = e => {
    e.preventDefault();
    const { dispatch, form } = this.props;
    const { current } = this.state;
    const pagination = this.pagination;
    const id = current ? current.id : '';

    setTimeout(() => this.addBtn.blur(), 0);
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      this.setState({
        done: true,
      });
      dispatch({
        type: 'hop/submit',
        payload: { 
          id, 
          ...fieldsValue,
          ...pagination
        },
      });
    });
  };

  deleteItem = id => {
    const { dispatch } = this.props;
    dispatch({
      type: 'hop/submit',
      payload: { 
        id,
        delete: true,
        ...this.pagination
      },
    });
  };

  render() {
    // const { enums } = this.props;
    const {
      hop: { hop },
      loading,
    } = this.props;
    const {
      form: { getFieldDecorator },
    } = this.props;
    const { visible, done, current = {attributes:{}} } = this.state;
    const { dispatch } = this.props;
    
    const editAndDelete = (key, currentItem) => {
      if (key === 'edit') this.showEditModal(currentItem);
      else if (key === 'delete') {
        Modal.confirm({
          title: 'Delete Hop',
          content: 'Are you sure you want to delete this hop?',
          okText: 'Yes',
          cancelText: 'No',
          onOk: () => this.deleteItem(currentItem.id),
        });
      }
    };

    const modalFooter = done
      ? { footer: null, onCancel: this.handleDone }
      : { okText: 'Ok', onOk: this.handleSubmit, onCancel: this.handleCancel };

    const Info = ({ title, value, bordered }) => (
      <div className={styles.headerInfo}>
        <span>{title}</span>
        <p>{value}</p>
        {bordered && <em />}
      </div>
    );

    const extraContent = (
      <div className={styles.extraContent}>
        <RadioGroup defaultValue="all">
          <RadioButton value="all">All</RadioButton>
          <RadioButton value="fermenting">Fermenting</RadioButton>
          <RadioButton value="hopped">Hopped</RadioButton>
          <RadioButton value="empty">Empty</RadioButton>
        </RadioGroup>
        <Search className={styles.extraContentSearch} placeholder="Search" onSearch={() => ({})} />
      </div>
    );
    const list = this;
    const paginationProps = {
      showSizeChanger: true,
      showQuickJumper: true,
      total: (hop.meta ? hop.meta.record_count : 0 ),
      onChange: function(page, pageSize) {
        list.pagination.page = page;
        list.pagination.limit = pageSize;
        dispatch({
          type: 'hop/fetch',
          payload: {
            ...list.pagination
          },
        });
      }
    };

    const ListContent = ({ data: {attributes: { name, created, fillpercent, status } }}) => (
      <div className={styles.listContent}>
        <div className={styles.listContentItem}>
          <span>Status</span>
          <p>{status}</p>
        </div>
        <div className={styles.listContentItem}>
          <span>Created</span>
          <p>{moment(created).fromNow()}</p>
        </div>
        <div className={styles.listContentItem}>
          <Progress percent={fillpercent * 100} status={status} strokeWidth={6} style={{ width: 180 }} />
        </div>
      </div>
    );

    const MoreBtn = props => (
      <Dropdown
        overlay={
          <Menu onClick={({ key }) => editAndDelete(key, props.current)}>
            <Menu.Item key="edit">Edit</Menu.Item>
            <Menu.Item key="delete">Delete</Menu.Item>
          </Menu>
        }
      >
        <a>
          Options <Icon type="down" />
        </a>
      </Dropdown>
    );

    const getModalContent = () => {
      if (done) {
        return (
          <Result
            type="success"
            title="Success"
            description="Hop created successfully!"
            actions={
              <Button type="primary" onClick={this.handleDone}>
                Ok
              </Button>
            }
            className={styles.formResult}
          />
        );
      }

      return (
        <Form onSubmit={this.handleSubmit}>
          <FormItem label="Name" {...this.formLayout}>
            {getFieldDecorator('name', {
              rules: [{ required: true, message: 'Please enter Hop Name' }],
              initialValue: current.attributes.name,
            })(<Input placeholder="Hop Name" />)}
          </FormItem>
        </Form>
      );
    };
    return (
      <PageHeaderWrapper>
        <div className={styles.standardList}>
          <Card bordered={false}>
            <Row>
              <Col sm={8} xs={24}>
                <Info title="Hops" value="?" bordered />
              </Col>
              <Col sm={8} xs={24}>
                <Info title="Fermenting" value="?" bordered />
              </Col>
              <Col sm={8} xs={24}>
                <Info title="Hopped" value="?" />
              </Col>
            </Row>
          </Card>

          <Card
            className={styles.listCard}
            bordered={false}
            title="Hops"
            style={{ marginTop: 24 }}
            bodyStyle={{ padding: '0 32px 40px 32px' }}
            extra={extraContent}
          >
            <Button
              type="dashed"
              style={{ width: '100%', marginBottom: 8 }}
              icon="plus"
              onClick={this.showModal}
              ref={component => {
                /* eslint-disable */
                this.addBtn = findDOMNode(component);
                /* eslint-enable */
              }}
            >
              Add
            </Button>
            <List
              size="large"
              rowKey="id"
              loading={loading}
              pagination={paginationProps}
              dataSource={hop}
              renderItem={item => (
                <List.Item
                  actions={[
                    <MoreBtn current={item} />,
                  ]}
                >
                  <List.Item.Meta
                    avatar={<Avatar src={item.logo} shape="square" size="large" />}
                    title={<a href={item.href}>{item.attributes.name}</a>}
                    description={item.subDescription}
                  />
                  <ListContent data={item} />
                </List.Item>
              )}
            />
          </Card>
        </div>
        <Modal
          title={done ? null : `任务${current ? '编辑' : '添加'}`}
          className={styles.standardListForm}
          width={640}
          bodyStyle={done ? { padding: '72px 0' } : { padding: '28px 0 0' }}
          destroyOnClose
          visible={visible}
          {...modalFooter}
        >
          {getModalContent()}
        </Modal>
      </PageHeaderWrapper>
    );
  }
}

export default HopList;
