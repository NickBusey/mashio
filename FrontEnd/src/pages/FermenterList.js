import React, { PureComponent } from 'react';
import { findDOMNode } from 'react-dom';
import moment from 'moment';
import { connect } from 'dva';
import {
  List,
  Card,
  Row,
  Col,
  Radio,
  Input,
  Progress,
  Button,
  Icon,
  Dropdown,
  Menu,
  Avatar,
  Modal,
  Form,
  DatePicker,
  Select,
} from 'antd';

import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import Result from '@/components/Result';

import styles from './List.less';

const FormItem = Form.Item;
const RadioButton = Radio.Button;
const RadioGroup = Radio.Group;
const Option = Select.Option;
const { Search, TextArea } = Input;

@connect(({ fermenter, loading }) => ({
  fermenter,
  loading: loading.models.fermenter,
}))
@Form.create()
class FermenterList extends PureComponent {
  state = { visible: false, done: false };

  formLayout = {
    labelCol: { span: 7 },
    wrapperCol: { span: 13 },
  };

  pagination = {
    sort: 'created',
    limit: 10,
    direction: 'desc',
    page: 1
  }

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({
      type: 'fermenter/index',
      payload: {
        ...this.pagination,
      },
    });
  }

  showModal = () => {
    this.setState({
      visible: true,
      current: undefined,
    });
  };

  showEditModal = item => {
    this.setState({
      visible: true,
      current: item,
    });
  };

  handleDone = () => {
    setTimeout(() => this.addBtn.blur(), 0);
    this.setState({
      done: false,
      visible: false,
    });
  };

  handleCancel = () => {
    setTimeout(() => this.addBtn.blur(), 0);
    this.setState({
      visible: false,
    });
  };

  handleSubmit = e => {
    e.preventDefault();
    const { dispatch, form } = this.props;
    const { current } = this.state;
    const pagination = this.pagination;
    const id = current ? current.id : '';

    setTimeout(() => this.addBtn.blur(), 0);
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      this.setState({
        done: true,
      });
      dispatch({
        type: 'fermenter/submit',
        payload: { 
          id, 
          ...fieldsValue,
          ...pagination,
          include: 'batches,batches.recipes'
        },
      });
    });
  };

  deleteItem = id => {
    const { dispatch } = this.props;
    dispatch({
      type: 'fermenter/submit',
      payload: { 
        id,
        delete: true,
        ...this.pagination
      },
    });
  };

  render() {
    // const { enums } = this.props;
    const {
      fermenter: { fermenter },
      loading,
    } = this.props;
    const {
      form: { getFieldDecorator },
    } = this.props;
    const { visible, done, current = {attributes:{}} } = this.state;
    const { dispatch } = this.props;
    
    const editAndDelete = (key, currentItem) => {
      if (key === 'edit') this.showEditModal(currentItem);
      else if (key === 'delete') {
        Modal.confirm({
          title: 'Delete Fermenter',
          content: 'Are you sure you want to delete this fermenter?',
          okText: 'Yes',
          cancelText: 'No',
          onOk: () => this.deleteItem(currentItem.id),
        });
      }
    };

    const modalFooter = done
      ? { footer: null, onCancel: this.handleDone }
      : { okText: 'Ok', onOk: this.handleSubmit, onCancel: this.handleCancel };

    const Info = ({ title, value, bordered }) => (
      <div className={styles.headerInfo}>
        <span>{title}</span>
        <p>{value}</p>
        {bordered && <em />}
      </div>
    );

    const extraContent = (
      <div className={styles.extraContent}>
        <RadioGroup defaultValue="all">
          <RadioButton value="all">All</RadioButton>
          <RadioButton value="fermenting">Fermenting</RadioButton>
          <RadioButton value="tapped">Tapped</RadioButton>
          <RadioButton value="empty">Empty</RadioButton>
        </RadioGroup>
        <Search className={styles.extraContentSearch} placeholder="Search" onSearch={() => ({})} />
      </div>
    );
    const list = this;
    const paginationProps = {
      showSizeChanger: true,
      showQuickJumper: true,
      total: (fermenter.meta ? fermenter.meta.record_count : 0 ),
      onChange: function(page, pageSize) {
        list.pagination.page = page;
        list.pagination.limit = pageSize;
        dispatch({
          type: 'fermenter/fetch',
          payload: {
            ...list.pagination
          },
        });
      }
    };

    const ListContent = ({ data: {attributes: { name, statusesText, typesText, 'fill-percent': fillpercent } }}) => (
      <div className={styles.listContent}>
        <div className={styles.listContentItem}>
          <span>Status</span>
          <p>{statusesText}</p>
        </div>
        <div className={styles.listContentItem}>
          <span>Type</span>
          <p>{typesText}</p>
        </div>
        <div className={styles.listContentItem}>
          <Progress percent={fillpercent} status="success" strokeWidth={6} style={{ width: 180 }} />
        </div>
      </div>
    );

    const MoreBtn = props => (
      <Dropdown
        overlay={
          <Menu onClick={({ key }) => editAndDelete(key, props.current)}>
            <Menu.Item key="edit">Edit</Menu.Item>
            <Menu.Item key="delete">Delete</Menu.Item>
          </Menu>
        }
      >
        <a>
          Options <Icon type="down" />
        </a>
      </Dropdown>
    );

    const getModalContent = () => {
      if (done) {
        return (
          <Result
            type="success"
            title="Success"
            description="Fermenter created successfully!"
            actions={
              <Button type="primary" onClick={this.handleDone}>
                Ok
              </Button>
            }
            className={styles.formResult}
          />
        );
      }

      const statuses = fermenter.enums ? fermenter.enums.statuses.map((d,idx) => <Option key={idx} value={idx}>{d}</Option>):[];
      const fermenter_types = fermenter.enums ? fermenter.enums.types.map((d,idx) => <Option key={idx} value={idx}>{d}</Option>):[];

      return (
        <Form onSubmit={this.handleSubmit}>
          <FormItem label="Name" {...this.formLayout}>
            {getFieldDecorator('name', {
              rules: [{ required: true, message: 'Please enter Fermenter Name' }],
              initialValue: current.attributes.name,
            })(<Input placeholder="Fermenter Name" />)}
          </FormItem>
          <FormItem label="Capacity" {...this.formLayout}>
            {getFieldDecorator('capacity', {
              rules: [{ required: true, message: 'Please enter Fermenter Capacity' }],
              initialValue: current.attributes.capacity,
            })(<Input placeholder="Fermenter Capacity" />)}
          </FormItem>
          <FormItem label="Status" {...this.formLayout}>
            {getFieldDecorator('status', {
              rules: [{ required: true, message: 'Please select a status' }],
              initialValue: current.attributes.status,
            })(
              <Select
                // key={item.id}
                placeholder="Select a Status"
                style={{ width: 200 }}
              >{statuses}</Select>
              )}
          </FormItem>
          {/* <FormItem label="Types" {...this.formLayout}>
            {getFieldDecorator('fermenter-type', {
              rules: [{ required: true, message: 'Please select a type' }],
              initialValue: current.attributes["fermenter-type"],
            })(
              <Select
                placeholder="Select a Type"
                style={{ width: 200 }}
              >{fermenter_types}</Select>
              )}
          </FormItem> */}
        </Form>
      );
    };
    return (
      <PageHeaderWrapper>
        <div className={styles.standardList}>
          <Card bordered={false}>
            <Row>
              <Col sm={8} xs={24}>
                <Info title="Fermenters" value="?" bordered />
              </Col>
              <Col sm={8} xs={24}>
                <Info title="Fermenting" value="?" bordered />
              </Col>
              <Col sm={8} xs={24}>
                <Info title="Tapped" value="?" />
              </Col>
            </Row>
          </Card>

          <Card
            className={styles.listCard}
            bordered={false}
            title="Fermenters"
            style={{ marginTop: 24 }}
            bodyStyle={{ padding: '0 32px 40px 32px' }}
            extra={extraContent}
          >
            <Button
              type="dashed"
              style={{ width: '100%', marginBottom: 8 }}
              icon="plus"
              onClick={this.showModal}
              ref={component => {
                /* eslint-disable */
                this.addBtn = findDOMNode(component);
                /* eslint-enable */
              }}
            >
              Add
            </Button>
            <List
              size="large"
              rowKey="id"
              loading={loading}
              pagination={paginationProps}
              dataSource={fermenter}
              renderItem={item => (
                <List.Item
                  actions={[
                    <MoreBtn current={item} />,
                  ]}
                >
                  <List.Item.Meta
                    avatar={<Avatar src={item.logo} shape="square" size="large" />}
                    title={<a href={item.href}>{item.attributes.name}</a>}
                    description={item.batch ? <a href={"/batches/view/"+item.batch.id}>{item.batch.attributes.name}</a> : "Empty"}
                  />
                  <ListContent data={item} />
                </List.Item>
              )}
            />
          </Card>
        </div>
        <Modal
          title={done ? null : `${current ? 'Add' : 'Edit'} Fermenter`}
          className={styles.standardListForm}
          width={640}
          bodyStyle={done ? { padding: '72px 0' } : { padding: '28px 0 0' }}
          destroyOnClose
          visible={visible}
          {...modalFooter}
        >
          {getModalContent()}
        </Modal>
      </PageHeaderWrapper>
    );
  }
}

export default FermenterList;
