import React, { PureComponent } from 'react';
import { findDOMNode } from 'react-dom';
import moment from 'moment';
import { connect } from 'dva';
import {
  List,
  Card,
  Row,
  Col,
  Radio,
  Input,
  Progress,
  Button,
  Icon,
  Dropdown,
  Menu,
  Avatar,
  Modal,
  Form,
  DatePicker,
  Select,
} from 'antd';

import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import Result from '@/components/Result';

import styles from './List.less';

const FormItem = Form.Item;
const RadioButton = Radio.Button;
const RadioGroup = Radio.Group;
const SelectOption = Select.Option;
const { Search, TextArea } = Input;

@connect(({ style, loading }) => ({
  style,
  loading: loading.models.style,
}))
@Form.create()
class StyleList extends PureComponent {
  state = { visible: false, done: false };

  formLayout = {
    labelCol: { span: 7 },
    wrapperCol: { span: 13 },
  };

  pagination = {
    sort: 'created',
    limit: 10,
    direction: 'desc',
    page: 1
  }

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({
      type: 'style/fetch',
      payload: {
        ...this.pagination,
        include: 'categories'
      },
    });
  }

  showModal = () => {
    this.setState({
      visible: true,
      current: undefined,
    });
  };

  showEditModal = item => {
    this.setState({
      visible: true,
      current: item,
    });
  };

  handleDone = () => {
    setTimeout(() => this.addBtn.blur(), 0);
    this.setState({
      done: false,
      visible: false,
    });
  };

  handleCancel = () => {
    setTimeout(() => this.addBtn.blur(), 0);
    this.setState({
      visible: false,
    });
  };

  handleSubmit = e => {
    e.preventDefault();
    const { dispatch, form } = this.props;
    const { current } = this.state;
    const pagination = this.pagination;
    const id = current ? current.id : '';

    setTimeout(() => this.addBtn.blur(), 0);
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      this.setState({
        done: true,
      });
      dispatch({
        type: 'style/submit',
        payload: { 
          id, 
          ...fieldsValue,
          ...pagination
        },
      });
    });
  };

  deleteItem = id => {
    const { dispatch } = this.props;
    dispatch({
      type: 'style/submit',
      payload: { 
        id,
        delete: true,
        ...this.pagination
      },
    });
  };

  handleSearch = value => {
    const { dispatch } = this.props;
    dispatch({
      type: 'style/fetch',
      payload: {
        ...this.pagination,
        filter: value,
        include: 'categories'
      },
    });
  };

  render() {
    const {
      style: { style },
      loading,
    } = this.props;
    console.log(this.props)
    console.log(style)
    const {
      form: { getFieldDecorator },
    } = this.props;
    const { visible, done, current = {attributes:{}} } = this.state;
    const { dispatch } = this.props;
    console.log(style)
    const editAndDelete = (key, currentItem) => {
      if (key === 'edit') this.showEditModal(currentItem);
      else if (key === 'delete') {
        Modal.confirm({
          title: 'Delete Style',
          content: 'Are you sure you want to delete this style?',
          okText: 'Yes',
          cancelText: 'No',
          onOk: () => this.deleteItem(currentItem.id),
        });
      }
    };

    const modalFooter = done
      ? { footer: null, onCancel: this.handleDone }
      : { okText: 'Ok', onOk: this.handleSubmit, onCancel: this.handleCancel };

    const Info = ({ title, value, bordered }) => (
      <div className={styles.headerInfo}>
        <span>{title}</span>
        <p>{value}</p>
        {bordered && <em />}
      </div>
    );

    const extraContent = (
      <div className={styles.extraContent}>
        <Search className={styles.extraContentSearch} placeholder="Search" onSearch={this.handleSearch} />
      </div>
    );
    const list = this;
    const paginationProps = {
      total: (style.meta ? style.meta.record_count : '?' ),
      onChange: function(page, pageSize) {
        list.pagination.page = page;
        list.pagination.limit = pageSize;
        dispatch({
          type: 'style/fetch',
          payload: {
            ...list.pagination
          },
          include: 'categories'
        });
      }
    };

    const ListContent = ({ data: {
      attributes: { 
        name,
        created,
        fillpercent,
        status,
        'abv-min': abvmin,
        'abv-max': abvmax,
        'og-min': ogmin,
        'og-max': ogmax,
        'fg-min': fgmin,
        'fg-max': fgmax,
        'ibu-min': ibumin,
        'ibu-max': ibumax
      },
      relationships: {
        category
      }
    }}) => (
      <div className={styles.listContent}>
        <div className={styles.listContentItem}>
          <span>OG</span>
          <p>{(ogmin?ogmin.toFixed(3):'?')} - {(ogmax?ogmax.toFixed(3):'?')}</p>
        </div>
        <div className={styles.listContentItem}>
          <span>FG</span>
          <p>{(fgmin?fgmin.toFixed(3):'?')} - {(fgmin?fgmin.toFixed(3):'?')}</p>
        </div>
        <div className={styles.listContentItem}>
          <span>IBU</span>
          <p>{(ibumin?ibumin.toFixed(3):'?')} - {(ibumax?ibumax.toFixed(3):'?')}</p>
        </div>
        <div className={styles.listContentItem}>
          <span>ABV</span>
          <p>{(abvmin?abvmin.toFixed(3):'?')} - {(abvmax?abvmax.toFixed(3):'?')}%</p>
        </div>
      </div>
    );

    return (
      <PageHeaderWrapper>
        <div className={styles.standardList}>
          <Card bordered={false}>
            <Row>
              <Col sm={8} xs={24}>
                <Info title="Styles" value={paginationProps.total} bordered />
              </Col>
              <Col sm={8} xs={24}>
                <Info title="Styles Brewed" value="?" bordered />
              </Col>
              <Col sm={8} xs={24}>
                <Info title="Styles Not Brewed" value="?" />
              </Col>
            </Row>
          </Card>

          <Card
            className={styles.listCard}
            bordered={false}
            title="Styles"
            style={{ marginTop: 24 }}
            bodyStyle={{ padding: '0 32px 40px 32px' }}
            extra={extraContent}
          >
            <List
              size="large"
              rowKey="id"
              loading={loading}
              pagination={paginationProps}
              dataSource={style}
              renderItem={item => (
                <List.Item>
                  <List.Item.Meta
                    avatar={<Avatar style={{ backgroundColor: item.attributes.color }} src={item.logo} shape="square" size="large"></Avatar>}
                    title={<a href={item.href}>{item.attributes["bjcp-number"]} - {item.attributes.name}</a>}
                    description={item.category.attributes.name}
                  />
                  <ListContent data={item} />
                </List.Item>
              )}
            />
          </Card>
        </div>
      </PageHeaderWrapper>
    );
  }
}

export default StyleList;
