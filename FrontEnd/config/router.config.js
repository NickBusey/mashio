export default [
  // user
  {
    path: '/user',
    component: '../layouts/UserLayout',
    routes: [
      { path: '/user', redirect: '/user/login' },
      { path: '/user/login', component: './User/Login' },
      { path: '/user/register', component: './User/Register' },
      { path: '/user/register-result', component: './User/RegisterResult' },
    ],
  },
  // app
  {
    path: '/',
    component: '../layouts/BasicLayout',
    Routes: ['src/pages/Authorized'],
    authority: ['admin', 'user'],
    routes: [
      // dashboard
      { path: '/', redirect: '/dashboard/analysis' },
      {
        path: '/dashboard',
        name: 'dashboard',
        icon: 'dashboard',
        routes: [
          {
            path: '/dashboard/batches',
            name: 'batches',
            component: './Batches/BatchList',
          },
          {
            path: '/dashboard/taps',
            name: 'taps',
            component: './TapList',
          },
          {
            path: '/dashboard/fermenters',
            name: 'fermenters',
            component: './FermenterList',
          },
        ],
      },
      {
        name: 'recipes',
        icon: 'check-circle-o',
        path: '/recipes',
        routes: [
          {
            path: '/recipes/browse',
            name: 'browse',
            component: './RecipeList',
          },
          {
            path: '/recipes/styles',
            name: 'browse-styles',
            component: './StyleList',
          },
        ],
      },
      {
        name: 'ingredients',
        icon: 'check-circle-o',
        path: '/ingredients',
        routes: [
          {
            path: '/ingredients/hops',
            name: 'hops',
            component: './HopList',
          },
        ],
      },

      // Hidden Items
      {
        path: '/batches/view/:id',
        name: 'batchesView',
        component: './Batches/BatchView',
        hideInMenu: true,
      },
      {
        component: '404',
      },
    ],
  },
];
